import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class EmailService {
  private apiUrl = environment.apiUrl

  constructor(private http: HttpClient) { }

  subscribe(email: string, name: string): Observable<Object> {
    const url = `${this.apiUrl}/mail/subscribe`;
    return this.http.post(url, {
      email: email,
      name: name
    }, {observe: 'body'});
  }
}

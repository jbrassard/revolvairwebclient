import { Component, OnInit } from '@angular/core';
import {NewUser} from '../models/new-user';
import {RegisterService} from '../services/register-service/register.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})

export class SignUpComponent implements OnInit {

  constructor(private registerService: RegisterService, private router : Router) { }
  model = new NewUser('', '', '', '', '');
  errors;

  ngOnInit() {
  }

  onSubmit() {
    const fullName = this.model.firstName.concat(' ', this.model.lastName);
    this.registerService.register(this.model.email, fullName, this.model.password).subscribe(
      result => console.log(result),
      error => this.errors = error.error.errors[Object.keys(error.error.errors)[0]][0],
      () => this.router.navigate(["sign-in"]));
  }


}

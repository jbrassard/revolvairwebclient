import {TestBed, inject, async} from '@angular/core/testing';

import { SensorService } from './sensor.service';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {StationService} from '../station-service/station.service';
import {environment} from '../../../environments/environment';

describe('SensorService', () => {
  let sensorService: SensorService;
  let apiUrl = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [SensorService]
    });
    sensorService = TestBed.get(SensorService);
  });

  it('should be created', inject([SensorService], (service: SensorService) => {
    expect(service).toBeTruthy();
  }));

  describe('getSensors()', () => {
    it('should get the sensors from apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let stationId = 9000;
          let expectedUrl = apiUrl+"/stations/" + stationId + "/sensors";

          //Act
          sensorService.getSensors(stationId).subscribe();

          //Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'GET'
          })
        })));

    it('should return an empty array if there was an error', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let expectedValue = [];
          let actualValue : Array<any>;
          let stationId = 9000;
          spyOn(http, 'get').and.returnValue(Observable.throw("oh no!"));

          //Act
          sensorService.getSensors(stationId).subscribe(
            stations => {
              actualValue = stations;
              expect(actualValue).toEqual(expectedValue);
            }
          );
        })))
  });

  describe('getLatestValues()', () => {
    it('should get the latest values from apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let stationId = 9000;
          let sensorId = 9000;
          let expectedUrl = apiUrl+"/stations/"+stationId+"/sensors/"+sensorId+"/readings/latest-values";

          //Act
          sensorService.getLatestValues(stationId, sensorId).subscribe();

          //Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'GET'
          })
        })));

    it('should return a new station if there was an error', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          let expectedValue = [];
          let actualValue : number[];
          let stationId = 9000;
          let sensorId = 9000;
          spyOn(http, 'get').and.returnValue(Observable.throw("oh no!"));

          //Act
          sensorService.getLatestValues(stationId, sensorId).subscribe(
            station => {
              actualValue = station;
              expect(actualValue).toEqual(expectedValue);
            }
          );
        })))
  })
});

export const environment = {
  production: true,
  apiUrl: 'https://airquebec.herokuapp.com/api'
};

import {TestBed, inject, async} from '@angular/core/testing';

import { EmailService } from './email.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {environment} from '../../../environments/environment';

describe('EmailService', () => {

  let emailService: EmailService;
  const apiUrl = environment.apiUrl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [EmailService]
    });
    emailService = TestBed.get(EmailService);
  });

  it('should be created', inject([EmailService], (service: EmailService) => {
    expect(service).toBeTruthy();
  }));

  describe('subscribe()', () => {
    it('should post email to apiUrl', async(
      inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
          //Arrange
          const expectedUrl = apiUrl + '/mail/subscribe';
          const email = 'ced.toup@hotmail.com';
          const name = 'Cédric Toupin';

          //Act
          emailService.subscribe(email, name).subscribe();

          //Assert
          backend.expectOne({
            url: expectedUrl,
            method: 'POST'
          });
      })));
  });
});
